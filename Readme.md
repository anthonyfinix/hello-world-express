### Hello World

this hosts a simple express app with reponse hello world on root get request

```javascript
app.get("/", (req, res) => res.send("Hello World"));
```

this is the docker configs

```Docker
FROM node:latest
WORKDIR /app
COPY package*.json .
RUN npm install
COPY . .
ENV PORT=3000
EXPOSE 3000
CMD [ "npm", "start" ]
```
